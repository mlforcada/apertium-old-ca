
# Notes per a instal·lar l’analitzador de català antic basat en Apertium

## Nota d'agraïment

El diccionari que usa aquest analitzador de català antic es basa en gran part en el llibre de Mercè Costa Clos i Maribel Tarrés Fernàndez 
*Diccionari del català antic* publicat per Edicions 62 el 1998. Les autores van cedir amablement els arxius a la Universitat d'Alacant 
per al desenvolupament, que es va fer vora l'any 1999. Aquest repositori conté una versió *recuperada* (l'any 2007, crec) d'aquell treball. 

## Avís

Les instruccions són per a un ordinador amb un sistema operatiu GNU/Linux de l’estil de Debian o Ubuntu. 

S'assumeixen coneixements de GNU/Linux i destresa amb la 
línia d'ordres.

Abans de res hem d'instal·lar el programari necessari. 

## Instal·lació d'Apertium

Seguint les instruccions d'[https://wiki.apertium.org/wiki/Install_Apertium_core_using_packaging](https://wiki.apertium.org/wiki/Install_Apertium_core_using_packaging), podem instal·lar els paquets bàsics de desenvolupament d'apertium:
```
# Triar-ne un:

# Nightly, (inestable, nou, useu aquest pràcticament en qualsevol cas):
curl -sS https://apertium.projectjj.com/apt/install-nightly.sh | sudo bash

# Release (estable però vell)
curl -sS https://apertium.projectjj.com/apt/install-release.sh | sudo bash

```
Tardarà una estona: encara que no necessitem tot, instal·la tot el que cal per a desenvolupar traductors automàtics amb `apertium`.

## Instal·lació de git

### Ubuntu
```
sudo apt update
```
(teclegem contrasenya)
```
sudo apt install git
```

### Debian
```
su
```
(teclegem contrasenya)

```
apt update
apt install git
exit
```


## Analitzador
### Descarregar les dades del repositori

Anem a un directori on es puga treballar i fem

```
git clone https://gitlab.com/mlforcada/apertium-old-ca
```

### *Compilar* l'analitzador
Ara hem de *compilar* les dades perquè puguen ser executades.
```
cd apertium-old-ca
./autogen.sh
make
```

## Executar l'analitzador
L'analitzador s'ha d'executar en la línia d'ordres. 

Abans de res, anem al directori `apertium-old-ca` on l'hem instal·lat.


### Provar l'analitzador
Provem primer si funciona:
```
echo "E vench fort enutjat, e molts altres vengren ab el" | apertium-destxt | lt-proc -a oldca-XX.automorf.bin | tr ' ' '\n'
```
Si tot va bé, hauria d'aparéixer aquesta eixida:
```
^E/I<conj>/Haver<verb><PresInd><p1><sg>$
^vench/venir<verb><PresInd><p1><sg>/venir<verb><IndefInd><p3><sg>$
^fort/fort<adv>/fort<adj><masc><sg>$
^enutjat/enutjar<verb><partpi><masc><sg>/enutjat<adj><masc><sg>$,
^e/i<conj>/haver<verb><PresInd><p1><sg>$
^molts/molt<adj><masc><pl>/molt<pron><indef><masc><pl>$
^altres/altre<pron><dem><fem><pl>/altre<pron><dem><masc><pl>$
^vengren/venir<verb><IndefInd><p3><pl>$
^ab/amb<prep>$
^el/el<art><def><masc><sg>/el<pron><ac><p3><masc><sg>/ell<pron><pers><p3><masc><sg>$.[][
]
```
On es veu com l'analitzador assigna a cada forma 
una o més anàlisis morfològiques separades amb `/`  (no desambigua; es podria provar el desambiguador de
català actual d'`apertium`, fent una canonada *frankenstein*).

En l'ordre, `echo` envia el text, `apertium-destxt` prepara
l'entrada per a enviar-la a l'analitzador, `lt-proc -a oldca-XX.automorf.bin`
executa com a analitzador (`-a`) 
el binari compilat que acaba en `.bin`, i `tr` fa que cada forma analitzada
vaja en una línia (en algun cas això pot no ser necessari).

Quan un mot és desconegut, el sistema repeteix el mot i el marca amb asterisc:
```
echo "Magalona hac una foldrapa" | apertium-destxt | lt-proc -a oldca-XX.automorf.bin | tr ' ' '\n'
```
L'eixida és:
```
^Magalona/*Magalona$
^hac/haver<verb><IndefInd><p1><sg>/haver<verb><IndefInd><p3><sg>$
^una/un<adj><fem><sg>/un<art><indef><fem><sg>$
^foldrapa/*foldrapa$.[][
]
```
Al final de les anàlisis apareix `$.[][]`; aquesta eixida està relacionada amb la gestió de final d'oració d'`apertium`. Es pot eliminar si molesta.

### Amb un fitxer
Sempre podem enviar un fitxer (per exemple  `text.txt` ) a l'analitzador i, per exemple,
arreplegar el resultat en un fitxer (per exemple `eixida.txt`):

```
cat text.txt |  apertium-destxt | lt-proc -a oldca-XX.automorf.bin | tr ' ' '\n' >eixida.txt

```

## Millorar l'analitzador

Per descomptat, si es coneixen els formats d'`apertium` és possible editar el diccionari `apertium-oldca-XX.oldca.dix` i tornar a compilar-lo amb `make`. Un bon lloc 
per a començar a llegir (encara que parla de tot un traductor i no només d'un analitzador) és 
`https://wiki.apertium.org/wiki/Apertium_New_Language_Pair_HOWTO`.

Si feu millores al diccionari 
i voleu contribuir-les a aquest repositori, feu-me una *issue* i en parlem.



